Development Board: NXP LPC55S69 <br />
Microphone: SparkFun Bob-12758 <br />
Library: Armadillo (calculating fft) <br />
Languages: C, C++ (QT)

This program is used for visualize sound on three plots. First is linear plot of sound. Second is FFT (Fast Fourier Transform) linear plot of sound in logarithmic scale. Third is FFT bar plot of sound. <br />

![Scheme](images/view.png)
![Scheme](images/pins.png)